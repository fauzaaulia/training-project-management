<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        $project = \App\Project::all();

        // dd($project);
        return view('project.index', compact('project'));
    }

    public function showCreate()
    {
        return view('project.create');
    }

    public function create()
    {
        # code tambah project
    }

    public function showUpdate($id)
    {
        return view('project.update');
    }
}
