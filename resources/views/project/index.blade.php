@extends('layout')

@section('content')
<h1 class="my-4">Project</h1>

<a class="btn btn-primary my-2" role="button" href="create">Tambah Project</a>
    <table class="table table-hover">
        <thead>
            <tr class="table-warning">
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Mulai</th>
            <th scope="col">Target</th>
            <th scope="col">Selesai</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($project as $p)
            <tr>
            <th scope="row">{{$loop->iteration}}</th>
                <td>{{$p['nama']}}</td>
                <td>{{$p['tanggal_mulai']}}</td>
                <td>{{$p['tanggal_target']}}</td>
                <td>{{$p['tanggal_selesai']}}</td>
                <td>
                    <button type="button" class="btn btn-primary btn-sm">Edit</button>
                    <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
