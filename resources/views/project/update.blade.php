@extends('layout')

@section('content')

<h1 class="my-4 text-center">Edit Project</h1>
<div class="row justify-content-center">
    <div class="col-6">
        <form>
            <div class="form-group">
                <label for="nama"><b>Nama</b></label>
                <input type="text" class="form-control" id="nama" placeholder="Nama Project">
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="tg_lmulai"><b>Tanggal Mulai</b></label>
                    <input type="text" class="form-control" id="tgl_mulai">
                </div>
                <div class="form-group col-md-6">
                    <label for="tgl_target"><b>Tanggal Target</b></label>
                    <input type="text" class="form-control" id="tgl_target">
                </div>
            </div>
                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
    </div>
</div>

@endsection
