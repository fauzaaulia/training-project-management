<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

//Route::get('/helo', 'ProjectController@index');

// Daftar Project
Route::get('/projects', 'ProjectController@index')->name('project.show');
// View: projects.index

// Tampilan tambah Project
Route::get('/projects/create', 'ProjectController@showCreate')->name('project.showcreate');
// View: projects.create

// Post tambah Project
Route::post('/projects/create', 'ProjectController@create')->name('project.create');
// Redirect ke /projects

// Tampilan update Project
Route::get('/projects/{id}', 'ProjectController@showUpdate')->name('project.showEdit');
// View: projects.update

// Post update Project
Route::post('/projects/:projectId', 'ProjectController@update');
// Redirect ke /projects/:projectId

// Post delete Project
Route::post('/projects/:projectId/delete', 'ProjectController@delete');
// Redirect ke /projects
